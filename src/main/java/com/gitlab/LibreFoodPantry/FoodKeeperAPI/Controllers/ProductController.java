package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Database;
import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models.Product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/products")
@Api()
public class ProductController {
	private Map<Integer, Product> productDb = Database.getProductDb();
	
	@GetMapping(value = "")
	@ApiOperation("Returns a list of all products")
	public ResponseEntity<Collection<Product>> getAllProducts() {
		if (productDb != null) {
			return new ResponseEntity<>(productDb.values(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation("Returns a specific product based on its unique identifier")
	public ResponseEntity<Product> getProductById(@PathVariable("id") Integer id) {
		if (productDb.containsKey(id)) {
			return new ResponseEntity<>(productDb.get(id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/{id}/name")
	@ApiOperation("Returns the name of a specific product based on its unique identifier")
	public ResponseEntity<String> getProductName(@PathVariable("id") Integer id) {
		if (productDb.containsKey(id)) {
			return new ResponseEntity<>(productDb.get(id).getName(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/category/{categoryId}")
	@ApiOperation("Returns a list of all products with the given category identifier")
	public ResponseEntity<Collection<Product>> getProductsWithCategoryId(@PathVariable("categoryId") Integer id) {
		List<Product> matchingProducts = new ArrayList<>();
		for (Product p : productDb.values()) {
			if (p.getCategoryId() == id) {
				matchingProducts.add(p);
			}
		}
		
		if (matchingProducts.size() > 0) { 
			return new ResponseEntity<>(matchingProducts, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
